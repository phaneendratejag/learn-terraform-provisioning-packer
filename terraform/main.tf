# Setup #
provider "aws" {
  region  = "eu-west-1"
  version = "~> 2.41"

}

terraform {
  backend "s3" {
    region  = "eu-west-1"
    encrypt = true
    bucket  = "takttime-terraform-states-eu-west-1"
    key     = "dev/terraform-entire-infra.tfstate"
  }
}

# Local variables for the module
locals {
  default_tags = {
    project     = var.project_name
    environment = var.environment
    owner       = var.owner
  }
}

# VPC
module "vpc" {
  source = "./modules/VPC"

  name_prefix = var.name_prefix
  vpc_name    = var.vpc_name
  vpc_cidr    = var.vpc_cidr

  availability_zones = var.availability_zones

  internet_gateway_name = var.internet_gateway_name
  nat_gateway_1a_name   = var.nat_gateway_1a_name
  nat_gateway_1b_name   = var.nat_gateway_1b_name

  dmz_public_subnets  = var.dmz_public_subnets
  app_private_subnets = var.app_private_subnets

  security_group_alb = var.security_group_alb
  security_group_app = var.security_group_app

  default_tags = local.default_tags
}

# EC2
module "ec2" {
  source = "./modules/EC2"

  name_prefix = var.name_prefix
  key_name    = var.key_name

  vpc_id = module.vpc.vpc_id

  dmz_public_subnet_ids  = module.vpc.dmz_public_subnet_ids
  app_private_subnet_ids = module.vpc.app_private_subnet_ids

  alb_sg_id = module.vpc.alb_sg_id
  app_sg_id = module.vpc.app_sg_id

  application_load_balancer_name = var.application_load_balancer_name

  #   web_launch_configuration  = var.web_launch_configuration
  #   app_launch_configuration  = var.app_launch_configuration
  #   fuse_launch_configuration = var.fuse_launch_configuration

  #   web_target_group_name        = var.web_target_group_name
  app_target_group_name = var.app_target_group_name
  #   app_fuse_target_group_name   = var.app_fuse_target_group_name

  #   auto_scaling_group_web  = var.auto_scaling_group_web
  #   auto_scaling_group_app  = var.auto_scaling_group_app
  #   auto_scaling_group_fuse = var.auto_scaling_group_fuse

  ec2_instances = var.ec2_instances
  default_tags  = local.default_tags
}

#S3
// module "tt-s3" {
//   source = "./modules/S3"

//   name_prefix = var.name_prefix

//   default_tags = local.default_tags
//   bucket_name  = var.s3_bucket_name
// }

#Transit Gateway
# module "transitGateway" {
#   source = "./modules/Transit Gateway"

#   name_prefix = var.name_prefix

#   name                   = var.transit_gateway_name
#   vpc_id                 = module.vpc.vpc_id
#   app_private_subnet_ids = module.vpc.app_private_subnet_ids
#   db_private_subnet_ids  = module.vpc.db_private_subnet_ids

#   default_tags = local.default_tags
# }
