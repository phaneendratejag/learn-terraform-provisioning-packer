# variables

## **************************************************************** ##
##                          Defaults                                ##
## **************************************************************** ##

## ---------------------------------------------------------------- ##
##                          NAME PREFIXES                           ##
## ---------------------------------------------------------------- ##
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

## ---------------------------------------------------------------- ##
##                              KEYS                                ##
## ---------------------------------------------------------------- ##
variable "key_name" {
  description = "Name of the key"
  type        = string
}

## ---------------------------------------------------------------- ##
##                      AVAILABILITY ZONES                          ##
## ---------------------------------------------------------------- ##
variable "availability_zones" {
  description = "A list of availability zones in the region"
  type        = list(string)
  default     = []
}

## ---------------------------------------------------------------- ##
##                              TAGS                                ##
## ---------------------------------------------------------------- ##
variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "owner" {
  type = string
}


## **************************************************************** ##
##                          modules/VPC                             ##
## **************************************************************** ##

## ---------------------------------------------------------------- ##
##                              VPC                                 ##
## ---------------------------------------------------------------- ##
variable "vpc_name" {
  description = "Name of the VPC"
  type        = string
  default     = "vpc_test"
}

variable "vpc_cidr" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "0.0.0.0/0"
}

## ---------------------------------------------------------------- ##
##                      Internet Gateway                            ##
## ---------------------------------------------------------------- ##
variable "internet_gateway_name" {
  description = "Name of the Internet Gateway"
  type        = string
}

## ---------------------------------------------------------------- ##
##                          NAT Gateway                             ##
## ---------------------------------------------------------------- ##
variable "nat_gateway_1a_name" {
  description = "Name of the NAT Gateway"
  type        = string
}

variable "nat_gateway_1b_name" {
  description = "Name of the NAT Gateway"
  type        = string
}

## ---------------------------------------------------------------- ##
##                      Security Groups                             ##
## ---------------------------------------------------------------- ##
variable "security_group_alb" {
  description = "Application Load Balancer Security group"
  type = object({
    name = string
    ingress_settings = list(object({
      description = string
      from_port   = number
      to_port     = number
      protocol    = string
    }))
  })
}

variable "security_group_app" {
  description = "App Security group"
  type = object({
    name = string
    ingress_settings = list(object({
      description = string
      from_port   = number
      to_port     = number
      protocol    = string
    }))
  })
}

## ---------------------------------------------------------------- ##
##                              SUBNETS                             ##
## ---------------------------------------------------------------- ##
variable "dmz_public_subnets" {
  description = "A list of public subnets inside the VPC"
  type = list(object({
    name = string
    cidr = string
  }))
  default = []
}

variable "app_private_subnets" {
  description = "A list of private subnets inside the VPC"
  type = list(object({
    name = string
    cidr = string
  }))
  default = []
}


## **************************************************************** ##
##                          modules/EC2                             ##
## **************************************************************** ##

## ---------------------------------------------------------------- ##
##                  Application Load Balancer                       ##
## ---------------------------------------------------------------- ##
variable "application_load_balancer_name" {
  description = "Name of the Application Load Balancer"
  type        = string
}


## ---------------------------------------------------------------- ##
##                            Instances                             ##
## ---------------------------------------------------------------- ##
variable "ec2_instances" {
  description = "EC2 instances"
  type = list(object({
    name          = string
    ami           = string
    instance_type = string
  }))
}

## ---------------------------------------------------------------- ##
##                          Target Groups                           ##
## ---------------------------------------------------------------- ##

variable "app_target_group_name" {
  description = "Name of the App Tomcat Target Group"
  type        = string
}

## **************************************************************** ##
##                          modules/S3                              ##
## **************************************************************** ##

## ---------------------------------------------------------------- ##
##                              S3                                  ##
## ---------------------------------------------------------------- ##
variable "s3_bucket_name" {
  description = "Name of the S3 bucket"
  type        = string
}


## **************************************************************** ##
##                      modules/Transit Gateway                     ##
## **************************************************************** ##

## ---------------------------------------------------------------- ##
##                          TRANSIT GATEWAY                         ##
## ---------------------------------------------------------------- ##
variable "transit_gateway_name" {
  description = "Name of the Transit Gateway"
  type        = string
}
