# prefix for all namespaces
name_prefix = "TT-Packer"

# key
key_name    = "TT-Key"

# vpc
vpc_name                = "VPC"
vpc_cidr                = "10.130.0.0/16"

# availability zones
availability_zones      = ["eu-west-1a", "eu-west-1b"]

# Internet Gateway
internet_gateway_name   = "IGW"

# NAT Gateway
nat_gateway_1a_name        = "NAT"
nat_gateway_1b_name        = "NAT"

# Route Table
route_table_for         = ""

# Secuirty Groups
security_group_alb = {
    name = "SG-ALB"
    ingress_settings = [{
        description     = "HTTP all traffic from outside"
        from_port       = 80
        to_port         = 80
        protocol        = "tcp"
    }, {
        description     = "HTTP all traffic on port 8080 from outside"
        from_port       = 8080
        to_port         = 8080
        protocol        = "tcp"
    }, {
        description     = "HTTP all traffic on port 8081 from outside"
        from_port       = 8081
        to_port         = 8081
        protocol        = "tcp"
    }]
}

security_group_app = {
    name = "SG-APP"
    ingress_settings = [{
        description     = "HTTP all traffic from outside"
        from_port       = 80
        to_port         = 80
        protocol        = "tcp"
    }, {
        description     = "HTTP all traffic on port 8080 from outside"
        from_port       = 8000
        to_port         = 8000
        protocol        = "tcp"
    }]
}
# subnets
dmz_public_subnets  = [{
    name = "dmz_public_subnet_1a",
    cidr = "10.130.1.0/24"
}, {
    name = "dmz_public_subnet_1b",
    cidr = "10.130.2.0/24"
}]

app_private_subnets = [{
    name = "app_private_subnets_1a",
    cidr = "10.130.5.0/24"
}, {
    name = "app_private_subnets_1b",
    cidr = "10.130.6.0/24"
}]
# load balancer
application_load_balancer_name = "ALB"

# target groups
app_target_group_name = "TG"
#S3
s3_bucket_name      = "tt-app"

#Transit Gateway
transit_gateway_name = "transit-gateway"

# EC2 instance
ec2_instances = [
    {
        name            = "Packer-splunk1"
        ami             = "ami-098f16afa9edf40be" # Red Hat Enterprise Linux 8 (HVM)
        instance_type   = "t2.micro"
    },
    {
        name            = "Packer-splunk2"
        ami             = "ami-098f16afa9edf40be" # Red Hat Enterprise Linux 8 (HVM)
        instance_type   = "t2.micro"
    }
]

# Tags
project_name        = "TaktTimeCloud"
environment         = "development"
owner               = "TaktTimeCloud"