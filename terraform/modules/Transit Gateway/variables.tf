# variables
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "name" {
  description = "Name of the Transit Gateway"
  type        = string
}

variable "vpc_id" {
  description = "ID of the VPC"
  type        = string
}

variable "app_private_subnet_ids" {
  description = "IDs of the APP private subnets"
  type        = list(string)
}

variable "db_private_subnet_ids" {
  description = "IDs of the DB private subnets"
  type        = list(string)
}

variable "default_tags" {
  description = "Default tags"
  type        = map(string)
}
