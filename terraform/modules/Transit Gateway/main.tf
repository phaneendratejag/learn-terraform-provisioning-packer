resource "aws_ec2_transit_gateway" "TT-TransitGateway" {
  description = "Takttime Transit Gateway"

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.name}"
    )
  )
}

resource "aws_ec2_transit_gateway_vpc_attachment" "TT-TGW_VPC_Attachment" {
  subnet_ids         = [var.app_private_subnet_ids[0], var.db_private_subnet_ids[1]]
  transit_gateway_id = aws_ec2_transit_gateway.TT-TransitGateway.id
  vpc_id             = var.vpc_id

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-gateway_attachment_${var.name}"
    )
  )
}
