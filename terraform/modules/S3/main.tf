resource "aws_s3_bucket" "s3" {
  bucket = "${var.bucket_name}-${lower(var.default_tags.project)}"
  acl    = "private"

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.bucket_name}"
    )
  )
}
