resource "aws_instance" "instance" {
  #   count = length(var.ec2_instances)

  ami                         = var.ami_id
  instance_type               = var.ec2_instances[0].instance_type
  vpc_security_group_ids      = var.security_group_ids
  subnet_id                   = var.private_subnet_id
  associate_public_ip_address = false
  key_name                    = "TTCS-KEY"

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.ec2_instances[0].name}"
    )
  )
}
