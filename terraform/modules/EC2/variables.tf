# variables
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "key_name" {
  description = "Name of the key"
  type        = string
}

variable "vpc_id" {
  description = "ID of the VPC"
  type        = string
}

variable "application_load_balancer_name" {
  description = "Name of the Application Load Balancer"
  type        = string
}

variable "app_target_group_name" {
  description = "Name of the App Tomcat Target Group"
  type        = string
}

variable "dmz_public_subnet_ids" {
  description = "IDs of the DMZ public subnets"
  type        = list(string)
}

variable "app_private_subnet_ids" {
  description = "IDs of the APP private subnets"
  type        = list(string)
}

variable "alb_sg_id" {
  description = "ID of the load balancer security group"
  type        = string
}

variable "app_sg_id" {
  description = "ID of the app security group"
  type        = string
}

variable "ec2_instances" {
  description = "EC2 instances"
  type = list(object({
    name          = string
    ami           = string
    instance_type = string
  }))
}

variable "default_tags" {
  description = "Default tags"
  type        = map(string)
}
