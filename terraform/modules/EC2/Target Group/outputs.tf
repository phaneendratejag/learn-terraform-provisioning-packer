output "tg_arn" {
  description = "The ARN of the target group"
  value       = aws_lb_target_group.instance_tg.arn
}
