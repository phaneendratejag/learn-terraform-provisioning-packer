resource "aws_lb_target_group" "instance_tg" {
  name     = var.name
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    path = "/"
  }

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.name}"
    )
  )
}
