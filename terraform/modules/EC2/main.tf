# module "web_target_group" {
#   source = "./Target Group"

#   name_prefix = var.name_prefix
#   name        = var.web_target_group_name
#   vpc_id      = var.vpc_id

#   default_tags = var.default_tags
# }

module "app_target_group" {
  source = "./Target Group"

  name_prefix = var.name_prefix
  name        = var.app_target_group_name
  vpc_id      = var.vpc_id

  default_tags = var.default_tags
}

# module "app_fuse_target_group" {
#   source = "./Target Group"

#   name_prefix = var.name_prefix
#   name        = var.app_fuse_target_group_name
#   vpc_id      = var.vpc_id

#   default_tags = var.default_tags
# }

module "app_lb" {
  source = "./Application Load Balancer"

  name_prefix           = var.name_prefix
  name                  = var.application_load_balancer_name
  alb_sg_id             = var.alb_sg_id
  dmz_public_subnet_ids = var.dmz_public_subnet_ids

  default_tags = var.default_tags
}

# resource "aws_alb_listener" "web_alb_listener" {
#   load_balancer_arn = module.app_lb.alb_arn
#   port              = 80
#   protocol          = "HTTP"

#   default_action {
#     target_group_arn = module.web_target_group.tg_arn
#     type             = "forward"
#   }
# }

resource "aws_alb_listener" "app_alb_listener" {
  load_balancer_arn = module.app_lb.alb_arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = module.app_target_group.tg_arn
    type             = "forward"
  }
}

# resource "aws_alb_listener" "fuse_alb_listener" {
#   load_balancer_arn = module.app_lb.alb_arn
#   port              = 8081
#   protocol          = "HTTP"

#   default_action {
#     target_group_arn = module.app_fuse_target_group.tg_arn
#     type             = "forward"
#   }
# }

# data "aws_ami" "amazon_linux_splunk" {
#   most_recent = true

#   filter {
#     name   = "name"
#     values = ["amzn-ami-hvm-2018.03.0.20200918.0-x86_64-ebs"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }

#   owners = ["137112412989"] # Canonical
# }

data "aws_ami" "amazon_linux_splunk" {
  most_recent = true

  filter {
    name   = "name"
    values = ["packer-image-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["438183327711"]
}


# data "aws_ami" "amazon_linux_db" {
#   most_recent = true

#   filter {
#     name   = "name"
#     values = ["elasticbeanstalk-amazon-2014.03.1-101d655db21e-docker-1.0.0-gpu"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }

#   owners = ["102837901569"] # Canonical
# }

# module "web_launch_configuration" {
#   source = "./Launch Configuration"

#   key_name = var.key_name

#   name_prefix   = var.name_prefix
#   name          = var.web_launch_configuration.name
#   image_id      = data.aws_ami.amazon_linux_splunk.id
#   instance_type = var.web_launch_configuration.instance_type

#   sg_id = var.web_sg_id
# }

# module "app_launch_configuration" {
#   source = "./Launch Configuration"

#   key_name = var.key_name

#   name_prefix   = var.name_prefix
#   name          = var.app_launch_configuration.name
#   image_id      = var.app_launch_configuration.image_id
#   instance_type = var.app_launch_configuration.instance_type

#   sg_id = var.app_sg_id
# }

# module "fuse_launch_configuration" {
#   source = "./Launch Configuration"

#   key_name = var.key_name

#   name_prefix   = var.name_prefix
#   name          = var.fuse_launch_configuration.name
#   image_id      = var.fuse_launch_configuration.image_id
#   instance_type = var.fuse_launch_configuration.instance_type

#   sg_id = var.app_sg_id
# }

# module "web_auto_scaling_group" {
#   source = "./Auto Scaling Group"

#   name_prefix        = var.name_prefix
#   auto_scaling_group = var.auto_scaling_group_web
#   lb_id              = module.app_lb.alb_id
#   lc_name            = module.web_launch_configuration.lc_name
#   target_group_arns  = [module.web_target_group.tg_arn]
#   private_subnet_ids = var.web_private_subnet_ids

#   default_tags = var.default_tags
# }

#Web-server instances
module "ec2_app_instances_private_subnet_0" {
  source = "./Instance"

  name_prefix        = var.name_prefix
  ec2_instances      = var.ec2_instances
  ami_id             = data.aws_ami.amazon_linux_splunk.id
  security_group_ids = [var.app_sg_id]
  private_subnet_id  = var.app_private_subnet_ids[0]
  default_tags       = var.default_tags
}


resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = module.app_target_group.tg_arn
  target_id        = module.ec2_app_instances_private_subnet_0.ec2_id
  port             = 80
}


# module "web_instance-1a" {
#   source = "./Instance"

#   name_prefix        = var.name_prefix
#   ec2_db             = var.ec2_web_1a
#   ami_id             = data.aws_ami.amazon_linux_splunk.id
#   security_group_ids = [var.web_sg_id]
#   private_subnet_id  = var.web_private_subnet_ids[0]
#   default_tags       = var.default_tags
# }

# module "web_instance-1b" {
#   source = "./Instance"

#   name_prefix        = var.name_prefix
#   ec2_db             = var.ec2_web_1b
#   ami_id             = data.aws_ami.amazon_linux_splunk.id
#   security_group_ids = [var.web_sg_id]
#   private_subnet_id  = var.web_private_subnet_ids[1]
#   default_tags       = var.default_tags
# }

# #App-server instances
# module "app_instance-1a" {
#   source = "./Instance"

#   name_prefix        = var.name_prefix
#   ec2_db             = var.ec2_app_1a
#   ami_id             = data.aws_ami.amazon_linux_splunk.id
#   security_group_ids = [var.app_sg_id]
#   private_subnet_id  = var.app_private_subnet_ids[0]
#   default_tags       = var.default_tags
# }

# module "app_instance-1b" {
#   source = "./Instance"

#   name_prefix        = var.name_prefix
#   ec2_db             = var.ec2_app_1b
#   ami_id             = data.aws_ami.amazon_linux_splunk.id
#   security_group_ids = [var.app_sg_id]
#   private_subnet_id  = var.app_private_subnet_ids[1]
#   default_tags       = var.default_tags
# }

# module "app_auto_scaling_group" {
#   source = "./Auto Scaling Group"

#   name_prefix        = var.name_prefix
#   auto_scaling_group = var.auto_scaling_group_app
#   lb_id              = module.app_lb.alb_id
#   lc_name            = module.app_launch_configuration.lc_name
#   target_group_arns  = [module.app_tomcat_target_group.tg_arn]
#   private_subnet_ids = var.app_private_subnet_ids

#   default_tags = var.default_tags
# }

# module "fuse_auto_scaling_group" {
#   source = "./Auto Scaling Group"

#   name_prefix        = var.name_prefix
#   auto_scaling_group = var.auto_scaling_group_fuse
#   lb_id              = module.app_lb.alb_id
#   lc_name            = module.fuse_launch_configuration.lc_name
#   target_group_arns  = [module.app_fuse_target_group.tg_arn]
#   private_subnet_ids = var.app_private_subnet_ids

#   default_tags = var.default_tags
# }


#DB-server instances
# module "db_instance-1a" {
#   source = "./Instance"

#   name_prefix        = var.name_prefix
#   ec2_db             = var.ec2_db_1a
#   ami_id             = data.aws_ami.amazon_linux_db.id
#   security_group_ids = [var.db_sg_id]
#   private_subnet_id  = var.db_private_subnet_ids[0]
#   default_tags       = var.default_tags
# }

# module "db_instance-1b-1" {
#   source = "./Instance"

#   name_prefix        = var.name_prefix
#   ec2_db             = var.ec2_db_1b_1
#   ami_id             = data.aws_ami.amazon_linux_db.id
#   security_group_ids = [var.db_sg_id]
#   private_subnet_id  = var.db_private_subnet_ids[1]
#   default_tags       = var.default_tags
# }

# module "db_instance-1b-2" {
#   source = "./Instance"

#   name_prefix        = var.name_prefix
#   ec2_db             = var.ec2_db_1b_2
#   ami_id             = data.aws_ami.amazon_linux_db.id
#   security_group_ids = [var.db_sg_id]
#   private_subnet_id  = var.db_private_subnet_ids[1]
#   default_tags       = var.default_tags
# }
