# variables
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "auto_scaling_group" {
  description = "Name of the Autoscaling group"
  type = object({
    name = string
    setting = object({
      min     = number
      max     = number
      desired = number
    })
  })
}
variable "lb_id" {
  description = "ID of the load balancer"
  type        = string
}

variable "lc_name" {
  description = "Name of the launch configuration"
  type        = string
}

variable "private_subnet_ids" {
  description = "IDs of subnet"
  type        = list(string)
}

variable "target_group_arns" {
  description = "ARNs of target groups"
  type        = list(string)
}

variable "default_tags" {
  description = "Default tags"
  type        = map(string)
}
