resource "aws_autoscaling_group" "web" {
  name = var.auto_scaling_group.name

  min_size         = var.auto_scaling_group.setting.min
  desired_capacity = var.auto_scaling_group.setting.desired
  max_size         = var.auto_scaling_group.setting.max

  health_check_grace_period = 300
  health_check_type         = "ELB"
  force_delete              = true
  launch_configuration      = var.lc_name
  vpc_zone_identifier       = var.private_subnet_ids
  target_group_arns         = var.target_group_arns

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  dynamic "tag" {
    for_each = var.default_tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  tag {
    key                 = "Name"
    value               = "${var.name_prefix}-${var.auto_scaling_group.name}"
    propagate_at_launch = true
  }

  timeouts {
    delete = "15m"
  }
}
