# variables
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "name" {
  description = "Load balancer name"
  type        = string
}

variable "alb_sg_id" {
  description = "Load balancer security group id"
  type        = string
}

variable "dmz_public_subnet_ids" {
  description = "DMZ public subnet IDs"
  type        = list(string)
}

variable "default_tags" {
  description = "Default tags"
  type        = map(string)
}
