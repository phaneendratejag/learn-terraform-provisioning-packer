output "alb_arn" {
  description = "The ARN of the load balancer"
  value       = aws_lb.alb.arn
}

output "alb_id" {
  description = "The ARN of the load balancer"
  value       = aws_lb.alb.id
}