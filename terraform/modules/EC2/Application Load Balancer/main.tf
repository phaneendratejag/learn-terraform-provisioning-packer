resource "aws_lb" "alb" {
  name               = var.name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.alb_sg_id]
  subnets            = var.dmz_public_subnet_ids

  enable_deletion_protection = false

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.name}"
    )
  )
}
