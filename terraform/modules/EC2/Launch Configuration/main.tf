resource "aws_launch_configuration" "launch_config" {
  name = var.name

  image_id      = var.image_id
  instance_type = var.instance_type
  key_name      = var.key_name

  security_groups             = [var.sg_id]
  associate_public_ip_address = false

  user_data = <<USER_DATA
#!/bin/bash
yum install httpd -y
service httpd start
chkconfig httpd on
mkdir /var/www/html
echo 'Hey!! This is ${var.name} server!' > /var/www/html/index.html
  USER_DATA

  lifecycle {
    create_before_destroy = true
  }
}
