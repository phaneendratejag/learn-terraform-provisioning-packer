output "lc_name" {
  description = "The ARN of the load balancer"
  value       = aws_launch_configuration.launch_config.name
}