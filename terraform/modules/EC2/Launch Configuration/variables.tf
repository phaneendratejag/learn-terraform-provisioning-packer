# variables
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "name" {
  description = "Name prefix for launch configuration"
  type        = string
}

variable "image_id" {
  description = "AMI image id for the instance"
  type        = string
}

variable "instance_type" {
  description = "Instance type"
  type        = string
}

variable "key_name" {
  description = "Key name"
  type        = string
}

variable "sg_id" {
  description = "Security group id"
  type        = string
}
