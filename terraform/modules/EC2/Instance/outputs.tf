output "ec2_id" {
  description = "The ID of EC2 instance"
  value       = aws_instance.instance.id
}
