# VPC
output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.this.id
}

output "dmz_public_subnet_ids" {
  description = "The ID of the subnet"
  value       = [module.dmz_public_subnet_1.subnet_id, module.dmz_public_subnet_2.subnet_id]
}

output "app_private_subnet_ids" {
  description = "The IDs of the subnet"
  value       = [module.app_private_subnet_1.subnet_id, module.app_private_subnet_2.subnet_id]
}

output "alb_sg_id" {
  description = "The ID of the ELB security group"
  value       = module.sg_alb.sg_id
}

output "app_sg_id" {
  description = "The ID of the ELB security group"
  value       = module.sg_app.sg_id
}
