# variables
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "vpc_name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = "vpc_test"
}

variable "vpc_cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  type        = string
  default     = "0.0.0.0/0"
}

variable "availability_zones" {
  description = "A list of availability zones in the region"
  type        = list(string)
  default     = []
}

variable "internet_gateway_name" {
  description = "Name of the Internet Gateway"
  type        = string
}

variable "nat_gateway_1a_name" {
  description = "Name of the NAT Gateway"
  type        = string
}

variable "nat_gateway_1b_name" {
  description = "Name of the NAT Gateway"
  type        = string
}

variable "dmz_public_subnets" {
  description = "A list of public subnets inside the VPC"
  type = list(object({
    name = string
    cidr = string
  }))
  default = []
}

variable "app_private_subnets" {
  description = "A list of private subnets inside the VPC"
  type = list(object({
    name = string
    cidr = string
  }))
  default = []
}

variable "security_group_alb" {
  description = "Application Load Balancer Security group"
  type = object({
    name = string
    ingress_settings = list(object({
      description = string
      from_port   = number
      to_port     = number
      protocol    = string
    }))
  })
}

variable "security_group_app" {
  description = "App Security group"
  type = object({
    name = string
    ingress_settings = list(object({
      description = string
      from_port   = number
      to_port     = number
      protocol    = string
    }))
  })
}

variable "default_tags" {
  type = map(string)
}
