output "sg_id" {
  description = "The ID of the Security group"
  value       = aws_security_group.sg.id
}
