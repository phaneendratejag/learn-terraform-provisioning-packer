resource "aws_security_group" "sg" {
  name        = "${var.name_prefix}-${var.name}"
  description = "Allow HTTP inbound traffic"
  vpc_id      = var.vpc_id

  dynamic "ingress" {
    iterator = setting
    for_each = var.ingress_setting
    content {
      description     = setting.value["description"]
      from_port       = setting.value["from_port"]
      to_port         = setting.value["to_port"]
      protocol        = "tcp"
      cidr_blocks     = setting.value["cidr_blocks"]
      security_groups = setting.value["security_groups"]
    }
  }

  ingress {
    description = "all traffic from itself"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self        = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.name}"
    )
  )
}
