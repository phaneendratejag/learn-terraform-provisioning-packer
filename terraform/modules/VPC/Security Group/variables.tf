variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "name" {
  description = "Name of the security group"
  type        = string
}

variable "vpc_id" {
  description = "ID of the VPC"
  type        = string
}

variable "ingress_setting" {
  type = list(object({
    description     = string
    from_port       = number
    to_port         = number
    protocol        = string
    cidr_blocks     = list(string)
    security_groups = list(string)
  }))
  description = "list of ingress ports"
  default     = []
}

variable "default_tags" {
  type = map(string)
}
