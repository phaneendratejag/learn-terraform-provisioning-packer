variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "vpc_id" {
  type = string
}

variable "gateway_id" {
  type    = string
  default = null
}

variable "nat_gateway_id" {
  type    = string
  default = null
}

variable "subnet_ids" {
  type = list(string)
}

variable "for" {
  type = string
}

variable "default_tags" {
  type = map(string)
}
