# routing table
resource "aws_route_table" "r" {
  vpc_id = var.vpc_id

  route {
    cidr_block     = "0.0.0.0/0"
    gateway_id     = var.gateway_id
    nat_gateway_id = var.nat_gateway_id
  }

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-RT-${var.for}"
    )
  )
}

# subnet association
resource "aws_route_table_association" "a" {
  count = length(var.subnet_ids)

  subnet_id      = var.subnet_ids[count.index]
  route_table_id = aws_route_table.r.id
}

