# variables
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "name" {
  description = "Name of the NAT Gateway"
  type        = string
}

variable "subnet_id" {
  type = string
}

variable "default_tags" {
  type = map(string)
}
