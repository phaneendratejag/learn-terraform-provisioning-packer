output "gateway_id" {
  description = "The ID of the NAT"
  value       = aws_nat_gateway.nat.id
}
