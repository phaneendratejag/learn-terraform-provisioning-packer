# NAT Gateway
resource "aws_eip" "my-test-eip" {
  vpc = true
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.my-test-eip.id
  subnet_id     = var.subnet_id

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.name}"
    )
  )
}
