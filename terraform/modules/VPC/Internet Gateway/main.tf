# Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = var.vpc_id

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.name}"
    )
  )
}
