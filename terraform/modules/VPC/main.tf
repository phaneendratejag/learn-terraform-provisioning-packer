# VPC
resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr

  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.vpc_name}"
    )
  )
}

# Internet Gateway
module "igw" {
  source = "./Internet Gateway"

  name_prefix = var.name_prefix
  name        = var.internet_gateway_name
  vpc_id      = aws_vpc.this.id

  default_tags = var.default_tags
}

# Subnets

## DMZ_PUBLIC_SUBNET_1
module "dmz_public_subnet_1" {
  source = "./Subnet"

  vpc_id = aws_vpc.this.id

  name_prefix       = var.name_prefix
  name              = var.dmz_public_subnets[0].name
  cidr              = var.dmz_public_subnets[0].cidr
  availability_zone = var.availability_zones[0]

  default_tags = var.default_tags
}

## DMZ_PUBLIC_SUBNET_2
module "dmz_public_subnet_2" {
  source = "./Subnet"

  vpc_id = aws_vpc.this.id

  name_prefix       = var.name_prefix
  name              = var.dmz_public_subnets[1].name
  cidr              = var.dmz_public_subnets[1].cidr
  availability_zone = var.availability_zones[1]

  default_tags = var.default_tags
}

## APP_PRIVATE_SUBNET_1
module "app_private_subnet_1" {
  source = "./Subnet"

  vpc_id = aws_vpc.this.id

  name_prefix       = var.name_prefix
  name              = var.app_private_subnets[0].name
  cidr              = var.app_private_subnets[0].cidr
  availability_zone = var.availability_zones[0]

  default_tags = var.default_tags
}

## APP_PRIVATE_SUBNET_2
module "app_private_subnet_2" {
  source = "./Subnet"

  vpc_id = aws_vpc.this.id

  name_prefix       = var.name_prefix
  name              = var.app_private_subnets[1].name
  cidr              = var.app_private_subnets[1].cidr
  availability_zone = var.availability_zones[1]

  default_tags = var.default_tags
}

# NAT gateway
module "nat_gateway_1a" {
  source = "./NAT Gateway"

  name_prefix = var.name_prefix
  name        = var.nat_gateway_1a_name
  subnet_id   = module.dmz_public_subnet_1.subnet_id

  default_tags = var.default_tags

}

module "nat_gateway_1b" {
  source = "./NAT Gateway"

  name_prefix = var.name_prefix
  name        = var.nat_gateway_1b_name
  subnet_id   = module.dmz_public_subnet_2.subnet_id

  default_tags = var.default_tags
}


# Routing table
module "rt_igw" {
  source = "./Route Table"

  vpc_id = aws_vpc.this.id

  name_prefix = var.name_prefix
  gateway_id  = module.igw.gateway_id
  for         = "igw"
  subnet_ids  = [module.dmz_public_subnet_1.subnet_id, module.dmz_public_subnet_2.subnet_id]

  default_tags = var.default_tags
}

module "rt_nat" {
  source = "./Route Table"

  vpc_id = aws_vpc.this.id

  name_prefix    = var.name_prefix
  nat_gateway_id = module.nat_gateway_1a.gateway_id
  for            = "nat"
  subnet_ids = [
    module.app_private_subnet_1.subnet_id, module.app_private_subnet_2.subnet_id,
  ]

  default_tags = var.default_tags
}

# security groups

## ELB
module "sg_alb" {
  source = "./Security Group"

  vpc_id = aws_vpc.this.id

  name_prefix = var.name_prefix
  name        = var.security_group_alb.name
  ingress_setting = [{
    description     = var.security_group_alb.ingress_settings[0].description
    from_port       = var.security_group_alb.ingress_settings[0].from_port
    to_port         = var.security_group_alb.ingress_settings[0].to_port
    protocol        = var.security_group_alb.ingress_settings[0].protocol
    cidr_blocks     = ["0.0.0.0/0"]
    security_groups = []
    }, {
    description     = var.security_group_alb.ingress_settings[1].description
    from_port       = var.security_group_alb.ingress_settings[1].from_port
    to_port         = var.security_group_alb.ingress_settings[1].to_port
    protocol        = var.security_group_alb.ingress_settings[1].protocol
    cidr_blocks     = ["0.0.0.0/0"]
    security_groups = []
    }, {
    description     = var.security_group_alb.ingress_settings[2].description
    from_port       = var.security_group_alb.ingress_settings[2].from_port
    to_port         = var.security_group_alb.ingress_settings[2].to_port
    protocol        = var.security_group_alb.ingress_settings[2].protocol
    cidr_blocks     = ["0.0.0.0/0"]
    security_groups = []
  }]

  default_tags = var.default_tags
}

## app server
module "sg_app" {
  source = "./Security Group"

  vpc_id = aws_vpc.this.id

  name_prefix = var.name_prefix
  name        = var.security_group_app.name
  ingress_setting = [{
    description     = var.security_group_app.ingress_settings[0].description
    from_port       = var.security_group_app.ingress_settings[0].from_port
    to_port         = var.security_group_app.ingress_settings[0].to_port
    protocol        = var.security_group_app.ingress_settings[0].protocol
    cidr_blocks     = []
    security_groups = [module.sg_alb.sg_id]
    }, {
    description     = var.security_group_app.ingress_settings[1].description
    from_port       = var.security_group_app.ingress_settings[1].from_port
    to_port         = var.security_group_app.ingress_settings[1].to_port
    protocol        = var.security_group_app.ingress_settings[1].protocol
    cidr_blocks     = []
    security_groups = [module.sg_alb.sg_id]
  }]

  default_tags = var.default_tags
}
