# variables
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "name" {
  description = "Name of the subnet"
  type        = string
  default     = ""
}

variable "cidr" {
  description = "CIDR block for the subnet"
  type        = string
  default     = ""
}

variable "availability_zone" {
  description = "availability zone to create subnet"
  type        = string
  default     = ""
}

variable "vpc_id" {
  description = "ID of the VPC"
  type = string
}

variable "default_tags" {
  description = "Default tags"
  type = map(string)
}
