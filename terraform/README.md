# Terraform for AWS

## Credentials

Set your access and secret keys from ```%USERPROFILE%/.aws/credentials``` under default category

```
    [default]
    aws_access_key_id=
    aws_secret_access_key=
```

## Generate a key-pair(PEM File)

Generate a key-pair(PEM file) named "TT-Key" from AWS console

## Backend

Create a bucket in your S3 with any name to store the state file, and update the bucket in ```main.tf``` for resource ```terraform backend s3```

## How to run (step by step)

To apply the infra

```
    terraform init
    terraform validate
    terraform plan -var-file=terraform.tfvars
    terraform apply -auto-approve -var-file=terraform.tfvars
```

To destroy the infra

```
    terraform destroy -auto-approve -var-file=terraform.tfvars
```

## Folder structure

```unicode
C:.
|   .gitignore
|   Env-diag.pdf
|   main.tf
|   outputs.tf
|   README.md
|   terraform.tfvars
|   variables.tf
|   VPC Architecture.txt
|
\---modules
    +---EC2
    |   |   main.tf
    |   |   variables.tf
    |   |
    |   +---Application Load Balancer
    |   |       main.tf
    |   |       outputs.tf
    |   |       variables.tf
    |   |
    |   +---Auto Scaling Group
    |   |       main.tf
    |   |       variables.tf
    |   |
    |   +---Instance
    |   |       main.tf
    |   |       variables.tf
    |   |
    |   +---Launch Configuration
    |   |       main.tf
    |   |       outputs.tf
    |   |       variables.tf
    |   |
    |   \---Target Group
    |           main.tf
    |           outputs.tf
    |           variables.tf
    |
    +---S3
    |       main.tf
    |       variables.tf
    |
    +---Transit Gateway
    |       main.tf
    |       variables.tf
    |
    \---VPC
        |   main.tf
        |   outputs.tf
        |   variables.tf
        |
        +---Internet Gateway
        |       main.tf
        |       outputs.tf
        |       variables.tf
        |
        +---NAT Gateway
        |       main.tf
        |       outputs.tf
        |       variables.tf
        |
        +---Route Table
        |       main.tf
        |       variables.tf
        |
        +---Security Group
        |       main.tf
        |       outputs.tf
        |       variables.tf
        |
        \---Subnet
                main.tf
                outputs.tf
                variables.tf
```